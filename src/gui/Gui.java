package gui;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;


public class Gui extends JFrame {
	private JLabel showProjectName ;
	private JButton endButton ;
	private String str ;
	private JTextArea showResults ;
	
	public Gui() {
		createFrame();
		
	}
	
	public void createFrame() {
		this.showProjectName = new JLabel("new project") ;
		this.showResults = new JTextArea("your results will be show here");
		this.endButton = new JButton("end program") ;
		setLayout(new BorderLayout());
		add(showProjectName , BorderLayout.NORTH);
		add(showResults , BorderLayout.CENTER);	
	}
	
	public void setProjectName(String s){
		showResults.setText(s);
	}
	
	public void setResult(String str) {
		this.str = str;
		showResults.setText(str);		
	}
	
	public void extendResult(String str) {
		this.str = this.str + "\n" + str ;
		showResults.setText(this.str);
	}
	
	public void setListener(ActionListener list){
		endButton.addActionListener(list);
	}
	
}